<?php
class Problem_Case {
  private $user_id;
  private $subject;
  private $message;
  private $category_id;
  private $status;
  private $open_date;
  private $close_date;
  private $source;
  private $problem_id;
  private $sql;

  public function __construct($sql) {
    $this->sql = $sql;
  }

  public static function InsertNew( $sql,
                                    $user_id,
                                    $subject,
                                    $message,
                                    $category_id,
                                    $status,
                                    $open_date,
                                    $close_date,
                                    $source,
                                    $problem_id) {
    $user_id      = mysqli_escape_string($sql, $user_id);
    $subject      = mysqli_escape_string($sql, $subject);
    $message      = mysqli_escape_string($sql, $message);
    $category_id  = mysqli_escape_string($sql, $category_id);
    $status       = mysqli_escape_string($sql, $status);
    $open_date    = mysqli_escape_string($sql, $open_date);
    $close_date   = mysqli_escape_string($sql, $close_date);
    $source       = mysqli_escape_string($sql, $source);
    $problem_id   = mysqli_escape_string($sql, $problem_id);
    global $db_prefix;

    $query        = "INSERT INTO " . $db_prefix . "_cases (user_id, subject, message, category_id, status, open_date, close_date, source, problem_id) VALUES
                  ('$user_id', '$subject', '$message', '$category_id', '$status', '$open_date', '$close_date', '$source', '$problem_id')";
    $result       = mysqli_query($sql, $query)
                     or die(mysqli_error($sql));
  }

  public static function GetAll($sql) {
    global $db_prefix;
    $query        = "SELECT * FROM " . $db_prefix . "_cases";
    $result       = mysqli_query($sql, $query)
                     or die(mysqli_error($sql));

    $cases        = array();
    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $case = new Problem_Case($sql);
      $case->loadFromResource($result, $i);
      array_push($cases, $case);
    }
    return $cases;
  }

  public static function GetForUserID($sql, $user_id) {
    global $db_prefix;
    $user_id      = mysqli_escape_string($sql, $user_id);

    $query        = "SELECT * FROM " . $db_prefix . "_cases WHERE user_id='$user_id'";
    $result       = mysqli_query($sql, $query)
                     or die(mysqli_error($sql));

    $cases        = array();
    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $case = new Problem_Case($sql);
      $case->loadFromResource($result, $i);
      array_push($cases, $case);
    }
    return $cases;
  }

  public static function GetForCaseID($sql, $case_id) {
    global $db_prefix;
    $case_id = mysqli_escape_string($sql, $case_id);

    $query        = "SELECT * FROM " . $db_prefix . "_cases WHERE ID='$case_id'";
    $result       = mysqli_query($sql, $query)
                     or die(mysqli_error($sql));

    if(mysqli_num_rows($result) == 0)
      return null;

    $case = new Problem_Case($sql);
    $case->loadFromResource($result);
    return $case;
  }

  public static function GetForSource($sql, $source) {
    global $db_prefix;
    $source = mysqli_escape_string($sql, $source);

    $query        = "SELECT * FROM " . $db_prefix . "_cases WHERE source='$source'";
    $result       = mysqli_query($sql, $query)
                     or die(mysqli_error($sql));

    $cases        = array();
    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $case = new Problem_Case($sql);
      $case->loadFromResource($result, $i);
      array_push($cases, $case);
    }
    return $cases;
  }

  public static function GetForProblemID($sql, $problem_id) {
    global $db_prefix;
    $problem_id = mysqli_escape_string($sql, $problem_id);

    $query        = "SELECT * FROM " . $db_prefix . "_cases WHERE problem_id='$problem_id'";
    $result       = mysqli_query($sql, $query)
                     or die(mysqli_error($sql));

    $cases        = array();
    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $case = new Problem_Case($sql);
      $case->loadFromResource($result, $i);
      array_push($cases, $case);
    }
    return $cases;
  }

  public static function GetForCategoryID($sql, $category_id) {
    global $db_prefix;
    $category_id = mysqli_escape_string($sql, $category_id);

    $query        = "SELECT * FROM " . $db_prefix . "_cases WHERE category_id='$category_id'";
    $result       = mysqli_query($sql, $query)
                     or die(mysqli_error($sql));

    $cases        = array();
    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $case = new Problem_Case($sql);
      $case->loadFromResource($result, $i);
      array_push($cases, $case);
    }
    return $cases;
  }

  public static function GetForStatus($sql, $status) {
    global $db_prefix;
    $status = mysqli_escape_string($sql, $status);

    $query        = "SELECT * FROM " . $db_prefix . "_cases WHERE status='$status'";
    $result       = mysqli_query($sql, $query)
                     or die(mysqli_error($sql));

    $cases        = array();
    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $case = new Problem_Case($sql);
      $case->loadFromResource($result, $i);
      array_push($cases, $case);
    }
    return $cases;
  }



  private function loadFromResource($resource, $index = 0) {
    $this->ID           = mysqli_result($resource, $index, "ID");
    $this->user_id      = mysqli_result($resource, $index, "user_id");
    $this->subject      = mysqli_result($resource, $index, "subject");
    $this->message      = mysqli_result($resource, $index, "message");
    $this->category_id  = mysqli_result($resource, $index, "category_id");
    $this->status       = mysqli_result($resource, $index, "status");
    $this->open_date    = mysqli_result($resource, $index, "open_date");
    $this->close_date   = mysqli_result($resource, $index, "close_date");
    $this->source       = mysqli_result($resource, $index, "source");
    $this->problem_id   = mysqli_result($resource, $index, "problem_id");
  }



  public function delete() {
    global $db_prefix;
    $query = "DELETE FROM " . $db_prefix . "_cases WHERE ID='" . $this->ID . "'";
    $result = mysqli_query($this->sql, $query)
      or die(mysqli_error($this->sql));
  }

  public function id() {
    return $this->ID;
  }

  public function user_id($newValue = null) {
    if($newValue == null) {
      return $this->user_id;
    }

    global $db_prefix;
    $newValue       = mysqli_escape_string($this->sql, $newValue);
    $this->user_id  = $newValue;

    $query          = "UPDATE " . $db_prefix . "_cases SET user_id='$newValue' WHERE ID='" . $this->ID . "'";
    $result         = mysqli_query($this->sql, $query)
                      or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function subject($newValue = null) {
    if($newValue == null) {
      return $this->subject;
    }

    global $db_prefix;
    $newValue       = mysqli_escape_string($this->sql, $newValue);
    $this->subject  = $newValue;

    $query          = "UPDATE " . $db_prefix . "_cases SET subject='$newValue' WHERE ID='" . $this->ID . "'";
    $result         = mysqli_query($this->sql, $query)
                        or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function message($newValue = null) {
    if($newValue == null) {
      return $this->message;
    }

    global $db_prefix;
    $newValue       = mysqli_escape_string($this->sql, $newValue);
    $this->message  = $newValue;

    $query          = "UPDATE " . $db_prefix . "_cases SET message='$newValue' WHERE ID='" . $this->ID . "'";
    $result         = mysqli_query($this->sql, $query)
                        or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function category_id($newValue = null) {
    if($newValue == null) {
      return $this->category_id;
    }

    global $db_prefix;
    $newValue           = mysqli_escape_string($this->sql, $newValue);
    $this->category_id  = $newValue;

    $query              = "UPDATE " . $db_prefix . "_cases SET category_id='$newValue' WHERE ID='" . $this->ID . "'";
    $result             = mysqli_query($this->sql, $query)
                            or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function status($newValue = null) {
    if($newValue == null) {
      return $this->status;
    }

    global $db_prefix;
    $newValue       = mysqli_escape_string($this->sql, $newValue);
    $this->status   = $newValue;

    $query          = "UPDATE " . $db_prefix . "_cases SET status='$newValue' WHERE ID='" . $this->ID . "'";
    $result         = mysqli_query($this->sql, $query)
                        or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function open_date($newValue = null) {
    if($newValue == null) {
      return $this->open_date;
    }

    global $db_prefix;
    $newValue         = mysqli_escape_string($this->sql, $newValue);
    $this->open_date  = $newValue;

    $query            = "UPDATE " . $db_prefix . "_cases SET open_date='$newValue' WHERE ID='" . $this->ID . "'";
    $result           = mysqli_query($this->sql, $query)
                          or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function close_date($newValue = null) {
    if($newValue == null) {
      return $this->close_date;
    }

    global $db_prefix;
    $newValue           = mysqli_escape_string($this->sql, $newValue);
    $this->close_date   = $newValue;

    $query              = "UPDATE " . $db_prefix . "_cases SET close_date='$newValue' WHERE ID='" . $this->ID . "'";
    $result             = mysqli_query($this->sql, $query)
                            or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function source($newValue = null) {
    if($newValue == null) {
      return $this->source;
    }

    global $db_prefix;
    $newValue       = mysqli_escape_string($this->sql, $newValue);
    $this->source   = $newValue;

    $query          = "UPDATE " . $db_prefix . "_cases SET source='$newValue' WHERE ID='" . $this->ID . "'";
    $result         = mysqli_query($this->sql, $query)
                        or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function problem_id($newValue = null) {
    if($newValue == null) {
      return $this->problem_id;
    }

    global $db_prefix;
    $newValue         = mysqli_escape_string($this->sql, $newValue);
    $this->problem_id = $newValue;

    $query            = "UPDATE " . $db_prefix . "_cases SET problem_id='$newValue' WHERE ID='" . $this->ID . "'";
    $result           = mysqli_query($this->sql, $query)
                          or die(mysqli_error($this->sql));

    return $newValue;
  }
}

?>
