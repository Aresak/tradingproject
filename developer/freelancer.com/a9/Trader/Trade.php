<?php
class Trade {
  private $ID;
  private $type;
  private $owner_id;
  private $date;
  private $owner_method;
  private $count;
  private $deposit;
  private $status;
  private $owner_status;
  private $opponent_id;
  private $opponent_status;
  private $opponent_method;
  private $owner_confirmation_time;
  private $opponent_confirmation_time;
  private $money_transfer_time;

  private $sql;

  public function __construct($sql) {
    $this->sql      = $sql;
  }


  public static function InsertNew( $sql,
                                    $type,
                                    $owner_id,
                                    $date,
                                    $owner_method,
                                    $count,
                                    $deposit,
                                    $status,
                                    $owner_status,
                                    $opponent_id,
                                    $opponent_status,
                                    $opponent_method,
                                    $owner_confirmation_time,
                                    $opponent_confirmation_time,
                                    $money_transfer_time) {
      $type                       = mysqli_escape_string($sql, $type);
      $owner_id                   = mysqli_escape_string($sql, $owner_id);
      $date                       = mysqli_escape_string($sql, $date);
      $owner_method               = mysqli_escape_string($sql, $owner_method);
      $count                      = mysqli_escape_string($sql, $count);
      $deposit                    = mysqli_escape_string($sql, $deposit);
      $status                     = mysqli_escape_string($sql, $status);
      $owner_status               = mysqli_escape_string($sql, $owner_status);
      $opponent_id                = mysqli_escape_string($sql, $opponent_id);
      $opponent_status            = mysqli_escape_string($sql, $opponent_status);
      $opponent_method            = mysqli_escape_string($sql, $opponent_method);
      $owner_confirmation_time    = mysqli_escape_string($sql, $owner_confirmation_time);
      $opponent_confirmation_time = mysqli_escape_string($sql, $opponent_confirmation_time);
      $money_transfer_time        = mysqli_escape_string($sql, $money_transfer_time);

      global $db_prefix;
      $query      = "INSERT INTO " . $db_prefix . "_trades (type, owner_id, `date`, owner_method, count, deposit, status, owner_status, opponent_id, opponent_status, opponent_method, owner_confirmation_time, opponent_confirmation_time, money_transfer_time) VALUES
      ('$type', '$owner_id', '$date', '$owner_method', '$count', '$deposit', '$status', '$owner_status', '$opponent_id', '$opponent_status', '$opponent_method', '$owner_confirmation_time', '$opponent_confirmation_time', '$money_transfer_time')";
      $result     = mysqli_query($sql, $query)
                      or die(mysqli_error($sql));
  }

  public static function GetAll($sql) {
    global $db_prefix;
    $query        = "SELECT * FROM " . $db_prefix . "_trades";
    $result       = mysqli_query($sql, $query)
                      or die(mysqli_error($sql));

    $trades       = array();

    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $trade = new Trade($sql);
      $trade->loadFromResource($result, $i);
      array_push($trades, $trade);
    }

    return $trades;
  }

  public static function GetForType($sql, $type) {
    global $db_prefix;
    $type         = mysqli_escape_string($sql, $type);

    $query        = "SELECT * FROM " . $db_prefix . "_trades WHERE `type`='$type'";
    $result       = mysqli_query($sql, $query)
                      or die(mysqli_error($sql));

    $trades       = array();

    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $trade = new Trade($sql);
      $trade->loadFromResource($result, $i);
      array_push($trades, $trade);
    }

    return $trades;
  }

  public static function GetForOwnerID($sql, $id) {
    global $db_prefix;
    $id           = mysqli_escape_string($sql, $id);

    $query        = "SELECT * FROM " . $db_prefix . "_trades WHERE owner_id='$id'";
    $result       = mysqli_query($sql, $query)
                      or die(mysqli_error($sql));

    $trades       = array();

    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $trade = new Trade($sql);
      $trade->loadFromResource($result, $i);
      array_push($trades, $trade);
    }

    return $trades;
  }

  public static function GetForStatus($sql, $status) {
    global $db_prefix;
    $status       = mysqli_escape_string($sql, $status);

    $query        = "SELECT * FROM " . $db_prefix . "_trades WHERE status='$status'";
    $result       = mysqli_query($sql, $query)
                      or die(mysqli_error($sql));

    $trades       = array();

    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $trade = new Trade($sql);
      $trade->loadFromResource($result, $i);
      array_push($trades, $trade);
    }

    return $trades;
  }

  public static function GetForOpponentID($sql, $id) {
    global $db_prefix;
    $id           = mysqli_escape_string($sql, $id);

    $query        = "SELECT * FROM " . $db_prefix . "_trades WHERE opponent_id='$id'";
    $result       = mysqli_query($sql, $query)
                      or die(mysqli_error($sql));

    $trades = array();

    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $trade = new Trade($sql);
      $trade->loadFromResource($result, $i);
      array_push($trades, $trade);
    }

    return $trades;
  }



  private function loadFromResource($resource, $index = 0) {
    $this->ID                         = mysqli_result($resource, $index, "ID");
    $this->type                       = mysqli_result($resource, $index, "type");
    $this->owner_id                   = mysqli_result($resource, $index, "owner_id");
    $this->date                       = mysqli_result($resource, $index, "date");
    $this->owner_method               = mysqli_result($resource, $index, "owner_method");
    $this->count                      = mysqli_result($resource, $index, "count");
    $this->deposit                    = mysqli_result($resource, $index, "deposit");
    $this->status                     = mysqli_result($resource, $index, "status");
    $this->owner_status               = mysqli_result($resource, $index, "owner_status");
    $this->opponent_id                = mysqli_result($resource, $index, "opponent_id");
    $this->opponent_status            = mysqli_result($resource, $index, "opponent_status");
    $this->opponent_method            = mysqli_result($resource, $index, "opponent_method");
    $this->owner_confirmation_time    = mysqli_result($resource, $index, "owner_confirmation_time");
    $this->opponent_confirmation_time = mysqli_result($resource, $index, "opponent_confirmation_time");
    $this->money_transfer_time        = mysqli_result($resource, $index, "money_transfer_time");
  }



  public function delete() {
    global $db_prefix;
    $query                = "DELETE FROM " . $db_prefix . "_trades WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));
  }

  public function id() {
    return $this->ID;
  }

  public function type($newValue = null) {
    global $db_prefix;
    if($newValue == null) {
      return $this->type;
    }

    $this->type           = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET type='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function owner_id($newValue = null) {
    if($newValue == null) {
      return $this->owner_id;
    }

    global $db_prefix;
    $this->owner_id       = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET owner_id='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function date($newValue = null) {
    if($newValue == null) {
      return $this->date;
    }

    global $db_prefix;
    $this->date           = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET date='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function owner_method($newValue = null) {
    if($newValue == null) {
      return $this->owner_method;
    }

    global $db_prefix;
    $this->owner_method   = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET owner_method='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function count($newValue = null) {
    if($newValue == null) {
      return $this->count;
    }

    global $db_prefix;
    $this->count          = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET count='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function deposit($newValue = null) {
    if($newValue == null) {
      return $this->deposit;
    }

    global $db_prefix;
    $this->deposit        = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET deposit='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function status($newValue = null) {
    if($newValue == null) {
      return $this->status;
    }

    global $db_prefix;
    $this->status         = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET status='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function owner_status($newValue = null) {
    if($newValue == null) {
      return $this->owner_status;
    }

    global $db_prefix;
    $this->owner_status   = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET owner_status='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function opponent_id($newValue = null) {
    if($newValue == null) {
      return $this->opponent_id;
    }

    global $db_prefix;
    $this->opponent_id    = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET opponent_id='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function opponent_status($newValue = null) {
    if($newValue == null) {
      return $this->opponent_status;
    }

    global $db_prefix;
    $this->opponent_status= $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET opponent_status='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function opponent_method($newValue = null) {
    if($newValue == null) {
      return $this->opponent_method;
    }

    global $db_prefix;
    $this->opponent_method= $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_trades SET opponent_method='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function owner_confirmation_time($newValue = null) {
    if($newValue == null) {
      return $this->owner_confirmation_time;
    }

    global $db_prefix;
    $this->owner_confirmation_time  = $newValue;
    $newValue                       = mysqli_escape_string($this->sql, $newValue);

    $query                          = "UPDATE " . $db_prefix . "_trades SET owner_confirmation_time='$newValue' WHERE ID='" . $this->ID . "'";
    $result                         = mysqli_query($this->sql, $query)
                                        or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function opponent_confirmation_time($newValue = null) {
    if($newValue == null) {
      return $this->opponent_confirmation_time;
    }

    global $db_prefix;
    $this->opponent_confirmation_time = $newValue;
    $newValue                         = mysqli_escape_string($this->sql, $newValue);

    $query                            = "UPDATE " . $db_prefix . "_trades SET opponent_confirmation_time='$newValue' WHERE ID='" . $this->ID . "'";
    $result                           = mysqli_query($this->sql, $query)
                                          or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function money_transfer_time($newValue = null) {
    if($newValue == null) {
      return $this->money_transfer_time;
    }

    global $db_prefix;
    $this->money_transfer_time  = $newValue;
    $newValue                   = mysqli_escape_string($this->sql, $newValue);

    $query                      = "UPDATE " . $db_prefix . "_trades SET money_transfer_time='$newValue' WHERE ID='" . $this->ID . "'";
    $result                     = mysqli_query($this->sql, $query)
                                    or die(mysqli_error($this->sql));

    return $newValue;
  }
}

?>
