<?php
class Case_Category {
  private $ID;
  private $name;
  private $sql;

  public function __construct($sql) {
    $this->sql    = $sql;
  }


  public static function InsertNew($sql, $name) {
    global $db_prefix;
    $name         = mysqli_escape_string($sql, $name);

    $query        = "INSERT INTO " . $db_prefix . "_case_categories (name) VALUES ('$name')";
    $result       = mysqli_query($sql, $query)
                      or die(mysqli_error($sql));
  }

  public static function GetAll($sql) {
    global $db_prefix;
    $query        = "SELECT * FROM " . $db_prefix . "_case_categories";
    $result       = mysqli_query($sql, $query)
                      or die(mysqli_error($sql));

    $categories   = array();

    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $category   = new Case_Category($sql);
      $category->loadFromResource($result, $i);
      array_push($categories, $category);
    }

    return $categories;
  }

  public static function GetForCategoryID($sql, $category_id) {
    global $db_prefix;
    $category_id  = mysqli_escape_string($sql, $category_id);

    $query        = "SELECT * FROM " . $db_prefix . "_case_categories WHERE ID='$category_id'";
    $result       = mysqli_query($sql, $query)
                      or die(mysqli_error($sql));

    if(mysqli_num_rows($result) == 0)
      return null;

    $category     = new Case_Category($sql);
    $category->loadFromResource($result);

    return $category;
  }


  private function loadFromResource($resource, $index = 0) {
    $this->ID     = mysqli_result($resource, $index, "ID");
    $this->name   = mysqli_result($resource, $index, "name");
  }


  public function delete() {
    global $db_prefix;
    $query        = "DELETE FROM " . $db_prefix . "_case_categories WHERE ID='" . $this->ID . "'";
    $result       = mysqli_query($this->sql, $query)
                      or die(mysqli_error($this->sql));
  }

  public function id() {
    return $this->ID;
  }

  public function name($newValue = null) {
    global $db_prefix;
    if($newValue == null) {
      return $this->name;
    }

    $newValue     = mysqli_escape_string($this->sql, $newValue);
    $this->name   = $newValue;

    $query        = "UPDATE " . $db_prefix . "_case_categories SET name='$newValue' WHERE ID='" . $this->ID . "'";
    $result       = mysqli_query($this->sql, $query)
                      or die(mysqli_error($this->sql));

    return $newValue;
  }
}

?>
