<?php
class User {
  private $ID;
  private $public_id;
  private $username;
  private $password_hash;
  private $password_salt;
  private $last_seen;
  private $points;

  private $sql;

  public function __construct($sql) {
    $this->sql        = $sql;
  }


  public static function InsertNew($sql, $public_id, $username, $password_hash, $password_salt, $last_seen, $points) {
    global $db_prefix;
    $public_id        = mysqli_escape_string($sql, $public_id);
    $username         = mysqli_escape_string($sql, $username);
    $password_hash    = mysqli_escape_string($sql, $password_hash);
    $password_salt    = mysqli_escape_string($sql, $password_salt);
    $last_seen        = mysqli_escape_string($sql, $last_seen);
    $points           = mysqli_escape_string($sql, $points);

    $query            = "INSERT INTO " . $db_prefix . "_users (public_id, username, password_hash, password_salt, last_seen, points) VALUES
                        ('$public_id', '$username', '$password_hash', '$password_salt', '$last_seen', '$points')";
    $result           = mysqli_query($sql, $query)
                          or die(mysqli_error($sql));
  }

  public static function GetForUserID($sql, $user_id) {
    global $db_prefix;
    $user_id          = mysqli_escape_string($sql, $user_id);

    $query            = "SELECT * FROM " . $db_prefix . "_users WHERE ID='$user_id'";
    $result           = mysqli_query($sql, $query)
                          or die(mysqli_error($sql));

    if(mysqli_num_rows($result) == 0)
      return null;

    $user             = new User($sql);
    $user->loadFromResource($result);

    return $user;
  }

  public static function GetForPublicID($sql, $public_id) {
    global $db_prefix;
    $public_id        = mysqli_escape_string($sql, $public_id);

    $query            = "SELECT * FROM " . $db_prefix . "_users WHERE public_id='$public_id'";
    $result           = mysqli_query($sql, $query)
                          or die(mysqli_error($sql));

    if(mysqli_num_rows($result) == 0)
      return null;

    $user             = new User($sql);
    $user->loadFromResource($result);

    return $user;
  }

  public static function GetForUsername($sql, $username) {
    global $db_prefix;
    $username         = mysqli_escape_string($sql, $username);

    $query            = "SELECT * FROM " . $db_prefix . "_users WHERE username='$username'";
    $result           = mysqli_query($sql, $query)
                          or die(mysqli_error($sql));

    if(mysqli_num_rows($result) == 0)
      return null;

    $user             = new User($sql);
    $user->loadFromResource($result);

    return $user;
  }

  public static function GeneratePassword($password_raw, $salt = null) {
    if($salt == null) {
      $salt           = md5(rand(1000000000, 999999999999));
    }

    return hash("sha256", $salt . $password_raw . "traderproject");
  }

  public static function GeneratePublicKey() {
    return rand(10000000, 99999999);
  }



  private function loadFromResource($resource, $index = 0) {
    $this->ID             = mysqli_result($resource, $index, "ID");
    $this->public_id      = mysqli_result($resource, $index, "public_id");
    $this->username       = mysqli_result($resource, $index, "username");
    $this->password_hash  = mysqli_result($resource, $index, "password_hash");
    $this->password_salt  = mysqli_result($resource, $index, "password_salt");
    $this->last_seen      = mysqli_result($resource, $index, "last_seen");
    $this->points         = mysqli_result($resource, $index, "points");
  }



  public function delete() {
    global $db_prefix;
    $query                = "DELETE FROM " . $db_prefix . "_users WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));
  }

  public function id() {
      return $this->ID;
  }

  public function public_id($newValue = null) {
    if($newValue == null) {
      return $this->public_id;
    }

    global $db_prefix;
    $this->public_id      = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_users SET public_id='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function username($newValue = null) {
    if($newValue == null) {
      return $this->username;
    }

    global $db_prefix;
    $this->username       = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_users SET username='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function password_hash($newValue = null) {
    if($newValue == null) {
      return $this->password_hash;
    }

    global $db_prefix;
    $this->password_hash  = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_users SET password_hash='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function password_salt($newValue = null) {
    if($newValue == null) {
      return $this->password_salt;
    }

    global $db_prefix;
    $this->password_salt  = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_users SET password_salt='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function last_seen($newValue = null) {
    if($newValue == null) {
      return $this->last_seen;
    }

    global $db_prefix;
    $this->last_seen      = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_users SET last_seen='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }

  public function points($newValue = null) {
    if($newValue == null) {
      return $this->points;
    }

    global $db_prefix;
    $this->points         = $newValue;
    $newValue             = mysqli_escape_string($this->sql, $newValue);

    $query                = "UPDATE " . $db_prefix . "_users SET points='$newValue' WHERE ID='" . $this->ID . "'";
    $result               = mysqli_query($this->sql, $query)
                              or die(mysqli_error($this->sql));

    return $newValue;
  }
}
?>
