<?php
class Case_Problem {
  private $ID;
  private $name;
  private $sql;


  public function __construct($sql) {
    $this->sql = $sql;
  }

  public static function InsertNew($sql, $name) {
    global $db_prefix;
    $name     = mysqli_escape_string($sql, $name);

    $query    = "INSERT INTO " . $db_prefix . "_case_problems (name) VALUES ('$name')";
    $result   = mysqli_query($sql, $query)
      or die(mysqli_error($sql));
  }

  public static function GetAll($sql) {
    global $db_prefix;
    $query    = "SELECT * FROM " . $db_prefix . "_case_problems";
    $result   = mysqli_query($sql, $query)
                  or die(mysqli_error($sql));

    $problems = array();

    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
      $problem = new Case_Problem($sql);
      $problem->loadFromResource($result, $i);
      array_push($problems, $problem);
    }

    return $problems;
  }

  public static function GetForProblemID($sql, $id) {
    global $db_prefix;
    $id       = mysqli_escape_string($sql, $id);

    $query    = "SELECT * FROM " . $db_prefix . "_case_problems WHERE ID='$id'";
    $result   = mysqli_query($sql, $query)
                or die(mysqli_error($sql));

    $problem  = new Case_Problem($sql);
    $problem->loadFromResource($result);

    return $problem;
  }



  private function loadFromResource($resource, $index = 0) {
    $this->ID   = mysqli_result($resource, $index, "ID");
    $this->name = mysqli_result($resource, $index, "name");
  }



  public function delete() {
    global $db_prefix;
    $query      = "DELETE FROM " . $db_prefix . "_case_problems WHERE ID='" . $this->ID . "'";
    $result     = mysqli_query($this->sql, $query)
                    or die(mysqli_error($this->sql));
  }

  public function id() {
    return $this->ID;
  }

  public function name($newValue = null) {
    global $db_prefix;
    if($newValue == null) {
      return $this->name;
    }

    $newValue   = mysqli_escape_string($this->sql, $newValue);
    $this->name = $newValue;

    $query      = "UPDATE " . $db_prefix . "_case_problems SET name='$newValue' WHERE ID='" . $this->ID . "'";
    $result     = mysqli_query($this->sql, $query)
                    or die(mysqli_error($this->sql));

    return $newValue;
  }
}

?>
