<?php

  require_once $path . "config.real.php";

  require_once $path . "Trader/Database.php";

  $sql = mysqli_connect($db_host, $db_user, $db_pass, $db_base)
    or die(mysqli_error($sql));

  require_once $path . "Trader/Utils.php";
  require_once $path . "Trader/User.php";
  require_once $path . "Trader/Trade.php";

  require_once $path . "Trader/Case.php";
  require_once $path . "Trader/Case_Problem.php";
  require_once $path . "Trader/Case_Category.php";

?>
