<?php

$path = "../";
require_once $path . "Trader/loader.php";

echo "<a href='./'>Go back</a><br><br><br>";


echo "Creating a new Case Problem 'The tests do not work!'<br>";
Case_Problem::InsertNew($sql, "The tests do not work!");
echo "Problem created.<br>Here is a list of all problems in the database:<br>";

$lid = 0;
foreach(Case_Problem::GetAll($sql) as $problem) {
  echo $problem->id() . " - " . $problem->name() . "<br>";
  $lid = $problem->id();
}

echo "Loading last problem $lid...<br>";

$problem = Case_Problem::GetForProblemID($sql, $lid);
echo "Problem loaded... " . $problem->id() . " - " . $problem->name() . "<br>";

echo "Renaming last problem $lid to 'Not that much issue!'...<br>";
$problem->name("Not that much issue!");

echo "Problem renamed.<br>Deleting the problem now...<br>";
$problem->delete();

echo "Problem deleted. Test is <b>OK</b>"
?>
