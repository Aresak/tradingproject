<?php

$path = "../";
require_once $path . "Trader/loader.php";

echo "<a href='./'>Go back</a><br><br><br>";


echo "Creating a new Category 'TestingCategory'<br>";
Case_Category::InsertNew($sql, "TestingCategory");

echo "Category created.<br>Displaying all categories:<br>";
$lid = 0;
foreach(Case_Category::GetAll($sql) as $cat) {
  echo "ID: " . $cat->id() . " - " . $cat->name() . "<br>";
  $lid = $cat->id();
}
echo "Displaying category ID $lid...<br>";

$cat = Case_Category::GetForCategoryID($sql, $lid);
echo "ID: " . $cat->id() . " - " . $cat->name() . "<br>";

echo "Changing name of category $lid to 'New Name of the category'<br>";
$cat->name("New Name of the category");
echo "Name changed. New name: " . $cat->name() . "<br>";
echo "Deleting the category now...<br>";
$cat->delete();

echo "Category deleted. Test is <b>OK</b>"
?>
