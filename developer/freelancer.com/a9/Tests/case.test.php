<?php

$path = "../";
require_once $path . "Trader/loader.php";

echo "<a href='./'>Go back</a><br><br><br>";

echo "Creating the test case...<br>";

Problem_Case::InsertNew($sql, 0, "Test case", "test message", 0, 0, 0, 0, 0, 0);

echo "Test case created. Here is list of all test cases:<br>";

$lid = 0;
foreach(Problem_case::GetAll($sql) as $case) {
  echo $case->id() . " - " . $case->subject() . " - " . $case->message() . "<br>";
  $lid = $case->id();
}

echo "Loading last test case for testing...<br>";

$case = Problem_Case::GetForCaseID($sql, $lid);

echo "Test case loaded. Now I will change just the subject for testing<br>";
$case->subject("Another subject");

echo "Subject name changed. Deleting the last test case...<br>";

$case->delete();

echo "Test case deleted. The test is <b>OK</b>";

?>
